package com.netto.jokenpoapi.controller;

import com.netto.jokenpoapi.model.Rodada;
import com.netto.jokenpoapi.model.service.AbstractCrudService;
import com.netto.jokenpoapi.model.service.RodadaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/rodadas")
@RestController
public class RodadaCrudController extends AbstractCrudController<Rodada> {

    @Autowired
    private RodadaService rodadaService;

    @Override
    protected AbstractCrudService<Rodada> getService() {
        return rodadaService;
    }

    @PostMapping(path = "start",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity start() {
        rodadaService.start();
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    @PostMapping(path = "{id}/encerrar",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity encerrar(@PathVariable("id") final Long id) {
        rodadaService.encerrar(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }


}