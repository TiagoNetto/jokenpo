package com.netto.jokenpoapi.controller;

import com.netto.jokenpoapi.model.IEntidade;
import com.netto.jokenpoapi.model.service.AbstractCrudService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Set;

public abstract class AbstractCrudController<T extends IEntidade> {

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAll() {
        Set<T> entidades = getService().findAll();
        return entidades.isEmpty() ? ResponseEntity.status(HttpStatus.NO_CONTENT).build() : ResponseEntity.ok(entidades);
    }

    @GetMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity get(@PathVariable("id") final Long id) {
        T entidade = getService().findById(id);
        return Objects.isNull(entidade) ? ResponseEntity.status(HttpStatus.NO_CONTENT).build() : ResponseEntity.ok(entidade);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@RequestBody final T entity) {
        return ResponseEntity.ok(getService().insert(entity));
    }


    @PutMapping(path = "{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@RequestBody final T entity, @PathVariable("id") final Long id) {
        if (!id.equals(entity.getId())) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
                    .body("ID do objeto difere do ID da URL");
        }
        getService().update(entity);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @DeleteMapping(path = "{id}")
    public ResponseEntity delete(@PathVariable("id") final Long id) {
        getService().remove(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }


    protected abstract AbstractCrudService<T> getService();

}
