package com.netto.jokenpoapi.controller;

import com.netto.jokenpoapi.model.Elemento;
import com.netto.jokenpoapi.model.Jogador;
import com.netto.jokenpoapi.model.service.AbstractCrudService;
import com.netto.jokenpoapi.model.service.JogadorService;
import com.netto.jokenpoapi.view.ElementoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/jogadores")
@RestController
public class JogadorController extends AbstractCrudController<Jogador> {

    @Autowired
    private JogadorService jogadorService;

    @Override
    protected AbstractCrudService<Jogador> getService() {
        return jogadorService;
    }

    @PostMapping(path = "{id}/jogar",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity jogar(@RequestBody final ElementoDto elemento, @PathVariable("id") final Long id) {
        jogadorService.jogar(id, Elemento.valueOf(elemento.getKey()));
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

}
