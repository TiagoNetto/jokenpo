package com.netto.jokenpoapi.model;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum Elemento implements IEnumType {

    SPOCK("SPOCK", Arrays.asList("PAPEL", "LAGARTO")),
    LAGARTO("LAGARTO", Arrays.asList("TESOURA", "PEDRA")),
    PEDRA("PEDRA", Arrays.asList("PAPEL", "SPOCK")),
    PAPEL("PAPEL", Arrays.asList("LAGARTO", "TESOURA")),
    TESOURA("TESOURA", Arrays.asList("SPOCK", "PEDRA"));

    private final String key;
    private final List<String> vencedores;

    private Elemento(final String key, final List<String> vencedores) {
        this.key = key;
        this.vencedores = vencedores;
    }

    public boolean isDerrotado(final List<Elemento> jogadas) {
        final List<String> keys = jogadas.stream().map(Elemento::getKey).collect(Collectors.toList());
        return getVencedores().stream().anyMatch(vencedor -> keys.contains(vencedor));
    }

    private List<String> getVencedores() {
        return vencedores;
    }

    @Override
    public String getKey() {
        return key;
    }
}
