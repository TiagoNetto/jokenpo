package com.netto.jokenpoapi.model;

import java.util.*;
import java.util.stream.Collectors;

public class Rodada implements IEntidade {
    private Long id;
    private Map<Jogador, Elemento> jogadas = new HashMap<Jogador, Elemento>();
    private Status status = Status.ANDAMENTO;
    private List<Jogador> vencedores = new ArrayList<>();

    public Rodada(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    private void setStatus(Status status) {
        this.status = status;
    }

    public void adicionarJogada(final Jogador jogador, Elemento elemento) {
        this.jogadas.put(jogador, elemento);
    }

    public void adicionarVencedores(final List<Jogador> jogadores) {
        this.vencedores.addAll(jogadores);
    }

    public void encerrar() {
        setStatus(Status.ENCERRADO);

        Set<Map.Entry<Jogador, Elemento>> jogadas = getJogadas().entrySet();
        List<Elemento> elementos = jogadas.stream().map(k -> k.getValue()).collect(Collectors.toList());
        adicionarVencedores(jogadas.stream()
                .filter(item -> !item.getValue().isDerrotado(elementos))
                .map(item -> item.getKey()).collect(Collectors.toList()));
    }

    public List<Jogador> getVencedores() {
        return vencedores;
    }

    public Map<Jogador, Elemento> getJogadas() {
        return jogadas;
    }

    private void setJogadas(Map<Jogador, Elemento> jogadas) {
        this.jogadas = jogadas;
    }

    @Override
    public Long getId() {
        return id;
    }

    private void setId(Long id) {
        this.id = id;
    }


    public enum Status implements IEnumType {
        ANDAMENTO("Andamento"),
        ENCERRADO("Finalizado");
        private final String key;

        private Status(final String key) {
            this.key = key;
        }

        @Override
        public String getKey() {
            return null;
        }
    }
}
