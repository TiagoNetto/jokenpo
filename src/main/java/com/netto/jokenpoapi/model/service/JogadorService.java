package com.netto.jokenpoapi.model.service;

import com.netto.jokenpoapi.model.Elemento;
import com.netto.jokenpoapi.model.Jogador;
import com.netto.jokenpoapi.repository.AbstractCrudRepository;
import com.netto.jokenpoapi.repository.JogadorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JogadorService extends AbstractCrudService<Jogador> {
    @Autowired
    private JogadorRepository jogadorRepository;
    @Autowired
    private RodadaService rodadaService;

    @Override
    protected AbstractCrudRepository getRepository() {
        return jogadorRepository;
    }

    public void jogar(final Long id, final Elemento elemento) {
        rodadaService.jogar(jogadorRepository.findById(id),elemento);
    }
}
