package com.netto.jokenpoapi.model.service;

import com.netto.jokenpoapi.model.IEntidade;
import com.netto.jokenpoapi.repository.AbstractCrudRepository;

import java.util.Objects;
import java.util.Set;

public abstract class AbstractCrudService<T extends IEntidade> {
    public T insert(T entidade) {
        getRepository().findAll().add(entidade);
        return entidade;
    }

    public Set<T> findAll() {
        return (Set<T>) getRepository().findAll();
    }

    public T findById(final Long id) {
        return getRepository().findById(id);
    }

    public void remove(final Long id) {
        T entidade = findById(id);
        if (Objects.nonNull(entidade)) {
            getRepository().findAll().remove(entidade);
        }
    }

    public T update(final T entidade) {
        T entidadeOld = findById(entidade.getId());
        if (Objects.isNull(entidadeOld)) {
            return null;
        }
        getRepository().findAll().remove(entidadeOld);
        return insert(entidade);
    }

    protected abstract AbstractCrudRepository<T> getRepository();
}
