package com.netto.jokenpoapi.model.service;

import com.netto.jokenpoapi.model.Elemento;
import com.netto.jokenpoapi.model.Jogador;
import com.netto.jokenpoapi.model.Rodada;
import com.netto.jokenpoapi.repository.AbstractCrudRepository;
import com.netto.jokenpoapi.repository.RodadaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class RodadaService extends AbstractCrudService<Rodada> {

    @Autowired
    private RodadaRepository rodadaRepository;

    @Override
    protected AbstractCrudRepository<Rodada> getRepository() {
        return rodadaRepository;
    }

    public void jogar(final Jogador jogador, final Elemento elemento) {
        Rodada rodada = rodadaRepository.findAll().iterator().next();
        rodada.adicionarJogada(jogador, elemento);
    }

    public void start() {
        Set<Rodada> rodadas = rodadaRepository.findAll();
        rodadas.add(new Rodada(rodadas.size() + 1L));
    }

    public void encerrar(final Long rodadaId) {
        final Rodada rodada = rodadaRepository.findById(rodadaId);
        rodada.encerrar();
    }
}
