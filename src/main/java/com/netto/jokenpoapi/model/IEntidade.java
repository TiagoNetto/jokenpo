package com.netto.jokenpoapi.model;

import java.io.Serializable;

public interface IEntidade extends Serializable {
    Long getId();
}
