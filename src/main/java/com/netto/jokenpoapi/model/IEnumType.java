package com.netto.jokenpoapi.model;

public interface IEnumType {
    String getKey();
}
