package com.netto.jokenpoapi.repository;

import com.netto.jokenpoapi.model.Jogador;
import org.springframework.stereotype.Component;

@Component
public class JogadorRepository extends AbstractCrudRepository<Jogador> {

}
