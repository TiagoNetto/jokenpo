package com.netto.jokenpoapi.repository;

import com.netto.jokenpoapi.model.Rodada;
import org.springframework.stereotype.Component;

@Component
public class RodadaRepository extends AbstractCrudRepository<Rodada> {
}
