package com.netto.jokenpoapi.repository;

import com.netto.jokenpoapi.model.IEntidade;

import java.util.HashSet;
import java.util.Set;

public abstract class AbstractCrudRepository<T extends IEntidade> {

    private Set<T> lista = new HashSet<T>();

    private Set<T> getLista() {
        return this.lista;
    }

    public Set<T> findAll() {
        return getLista();
    }

    public T findById(final Long id) {
        return getLista().stream().filter(lista -> lista.getId().equals(id)).findFirst().orElse(null);
    }
}
